logLevel := Level.Info

addSbtPlugin("org.foundweekends" % "sbt-bintray"  % "0.6.1")
addSbtPlugin("com.github.gseitz" % "sbt-release"  % "1.0.13")
addSbtPlugin("org.scalameta"     % "sbt-scalafmt" % "2.4.2")
