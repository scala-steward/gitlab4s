package io.morgaroth.gitlabclient

case class GitlabRestAPIConfig(
    debug: Boolean = false,
)
